#!/usr/bin/python
##################################################
#    ____     _   ____          __           __  #
#   / __/__ _(_) / __/__ ____  / /____  ___ / /  #
#  _\ \/ _ `/ / _\ \/ _ `/ _ \/ __/ _ \(_-</ _ \ #
# /___/\_,_/_/ /___/\_,_/_//_/\__/\___/___/_//_/ #
#                                                #
#             TWITTER : @SASH243                 #
##################################################
import cgi,cgitb
import re
import socket,struct
from modules import *
from json2html import *
from concurrent.futures import *

form = cgi.FieldStorage()
ip = form.getvalue('ip')
# ip = '8.8.8.8'

if re.match(r'^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$',ip):

  iplong = struct.unpack("!I", socket.inet_aton(ip))[0]

  if ((167772160 < iplong < 184549375) | (2886729728 < iplong < 2887778303) | (3232235520 < iplong < 3232301055)):
    response = '<p><h1>This is <a href="https://tools.ietf.org/html/rfc1918">RFC 1918</a> IP Address</h1></p>'
    pass

  else:
    with ProcessPoolExecutor(max_workers=6) as executor:
      futures = {
                 executor.submit(VirusTotalInfo, ip),
                 executor.submit(alienvault_otx, ip),
                 executor.submit(IPvoidInfo, ip),
                 executor.submit(MalwareDomainListInfo, ip),
				 executor.submit(MalwareTrafficAnalysis, ip),
                 executor.submit(DshieldInfo, ip),
                 executor.submit(ShodanInfo, ip),
                 executor.submit(WhoisInfo, ip),
                 executor.submit(RobtexGraph, ip)
                }

      response = ''
      for future in as_completed(futures):
        response = response + future.result()

    pass
  pass

else :
  response = '''<h1>Please enter a valid IP Address</h1>'''
  pass



print "Content-type:text/html\r\n\r\n"
print '<html>'
print '<head>'
print '<title>IP Reputation Results</title>'
print '''<script type="text/javascript">
var collapseDivs, collapseLinks;

function createDocumentStructure (tagName) {
  if (document.getElementsByTagName) {
    var elements = document.getElementsByTagName(tagName);
    collapseDivs = new Array(elements.length);
    collapseLinks = new Array(elements.length);
    for (var i = 0; i < elements.length; i++) {
      var element = elements[i];
      var siblingContainer;
      if (document.createElement && 
          (siblingContainer = document.createElement('div')) &&
          siblingContainer.style) 
      {
        var nextSibling = element.nextSibling;
        element.parentNode.insertBefore(siblingContainer, nextSibling);
        var nextElement = elements[i + 1];
        while (nextSibling != nextElement && nextSibling != null) {
          var toMove = nextSibling;
          nextSibling = nextSibling.nextSibling;
          siblingContainer.appendChild(toMove);
        }
        siblingContainer.style.display = 'none';
        
        collapseDivs[i] = siblingContainer;
        
        createCollapseLink(element, siblingContainer, i);
      }
      else {
        // no dynamic creation of elements possible
        return;
      }
    }
    createCollapseExpandAll(elements[0]);
  }
}

function createCollapseLink (element, siblingContainer, index) {
  var span;
  if (document.createElement && (span = document.createElement('span'))) {
    span.appendChild(document.createTextNode(String.fromCharCode(160)));
    var link = document.createElement('a');
    link.collapseDiv = siblingContainer;
    link.href = '#';
    link.appendChild(document.createTextNode('expand'));
    link.onclick = collapseExpandLink;
    collapseLinks[index] = link;
    span.appendChild(link);
    element.appendChild(span);
  }
}

function collapseExpandLink (evt) {
  if (this.collapseDiv.style.display == '') {
    this.parentNode.parentNode.nextSibling.style.display = 'none';
    this.firstChild.nodeValue = 'expand';
  }
  else {
    this.parentNode.parentNode.nextSibling.style.display = '';
    this.firstChild.nodeValue = 'collapse';
  }

  if (evt && evt.preventDefault) {
    evt.preventDefault();
  }
  return false;
}

function createCollapseExpandAll (firstElement) {
  var div;
  if (document.createElement && (div = document.createElement('div'))) {
    var link = document.createElement('a');
    link.href = '#';
    link.appendChild(document.createTextNode('expand all'));
    link.onclick = expandAll;
    div.appendChild(link);
    div.appendChild(document.createTextNode(' '));
    link = document.createElement('a');
    link.href = '#';
    link.appendChild(document.createTextNode('collapse all'));
    link.onclick = collapseAll;
    div.appendChild(link);
    firstElement.parentNode.insertBefore(div, firstElement);
  }
}

function expandAll (evt) {
  for (var i = 0; i < collapseDivs.length; i++) {
    collapseDivs[i].style.display = '';
    collapseLinks[i].firstChild.nodeValue = 'collapse';
  }
  
  if (evt && evt.preventDefault) {
    evt.preventDefault();
  }
  return false;
}

function collapseAll (evt) {
  for (var i = 0; i < collapseDivs.length; i++) {
    collapseDivs[i].style.display = 'none';
    collapseLinks[i].firstChild.nodeValue = 'expand';
  }
  
  if (evt && evt.preventDefault) {
    evt.preventDefault();
  }
  return false;
}
</script>
<script type="text/javascript">
window.onload = function (evt) {
  createDocumentStructure('h2');
}
</script>'''
print '</head>'
print '<body>'

print response

print '</body>'
print '</html>'
