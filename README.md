# Look-Up Tool #

LookUp tool is a centralized page to get various threat information about an IP address. Which can be easily integrated into context menus of Numerous tools like SIEMs and Any other Investigation tools.

### Hosting an IP-LookUp Instance###

* Getting the package
```
#!shell

sudo git clone https://bitbucket.org/ssanthosh243/ip-lookup-docker.git
cd ip-lookup-docker/

```

* Building the docker (Don't forget the . in the end)

```
#!shell

sudo docker build -t lookup .
```

* Running the docker instance

```
#!shell

sudo docker run  -it --name Lookup -p 80:80 \
            -v /PATH/TO/CONFIGURATIONFILE/:/data/lookup-tool \
            -d lookup
```

* Edit the configuration file and add the Api keys from respective sites
* you can access the page http://<IP_Address>/cgi-bin/query.py?ip=<IP_to_look_for>

### Who do I talk to? ###

* [Sai Santosh](https://in.linkedin.com/in/sai-santosh-d-41443630)